
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8" />
    <title>Basic App: Hello World &#8212; GeneFlow 1.11.0 documentation</title>
    <link rel="stylesheet" href="../../_static/classic.css" type="text/css" />
    <link rel="stylesheet" href="../../_static/pygments.css" type="text/css" />
    
    <script type="text/javascript" id="documentation_options" data-url_root="../../" src="../../_static/documentation_options.js"></script>
    <script type="text/javascript" src="../../_static/jquery.js"></script>
    <script type="text/javascript" src="../../_static/underscore.js"></script>
    <script type="text/javascript" src="../../_static/doctools.js"></script>
    <script type="text/javascript" src="../../_static/language_data.js"></script>
    
    <link rel="index" title="Index" href="../../genindex.html" />
    <link rel="search" title="Search" href="../../search.html" />
    <link rel="next" title="One-Step Workflow: Hello World" href="one-step-workflow.html" />
    <link rel="prev" title="GeneFlow Tutorials" href="../tutorials.html" /> 
  </head><body>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../../genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="one-step-workflow.html" title="One-Step Workflow: Hello World"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="../tutorials.html" title="GeneFlow Tutorials"
             accesskey="P">previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="../../index.html">GeneFlow 1.11.0 documentation</a> &#187;</li>
          <li class="nav-item nav-item-1"><a href="../tutorials.html" accesskey="U">GeneFlow Tutorials</a> &#187;</li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <div class="section" id="basic-app-hello-world">
<h1>Basic App: Hello World<a class="headerlink" href="#basic-app-hello-world" title="Permalink to this headline">¶</a></h1>
<p>GeneFlow workflows are composed of applications, or apps, which are modular pieces of functionality. Apps are ideally designed so that they can be combined to produce useful data analysis workflows. This tutorial covers the creation of a basic GeneFlow app that prints “Hello World!” to a text file. It is meant to be an introduction to the basic features of GeneFlow apps and does not cover all features. Advanced GeneFlow app features will be covered in later tutorials.</p>
<div class="section" id="configure-the-environment">
<h2>Configure the Environment<a class="headerlink" href="#configure-the-environment" title="Permalink to this headline">¶</a></h2>
<div class="section" id="load-or-install-geneflow">
<h3>Load or Install GeneFlow<a class="headerlink" href="#load-or-install-geneflow" title="Permalink to this headline">¶</a></h3>
<p>Configure the Linux environment by loading or installing GeneFlow. To check if GeneFlow is available, use the following command:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>geneflow --help
</pre></div>
</div>
<p>If it’s available, you should see GeneFlow’s usage instructions:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>usage: geneflow [-h] [--log_level LOG_LEVEL] [--log_file LOG_FILE]
                {add-apps,add-workflows,help,init-db,install-workflow,make-app,migrate-db,run,run-pending}
                ...

GeneFlow CLI

positional arguments:
  {add-apps,add-workflows,help,init-db,install-workflow,make-app,migrate-db,run,run-pending}
                        Functions
    add-apps            add apps to database
    add-workflows       add workflows to database
    help                GeneFlow workflow help
    init-db             initialize database
    install-workflow    install workflow
    make-app            make app from templates
    migrate-db          migrate database
    run                 run a GeneFlow workflow
    run-pending         run pending workflow jobs

optional arguments:
  -h, --help            show this help message and exit
  --log_level LOG_LEVEL
                        logging level
  --log_file LOG_FILE   log file
</pre></div>
</div>
<p>However, if it’s not installed, you’ll get an error message like this:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>-bash: geneflow: command not found
</pre></div>
</div>
<p>If your system is configured with modules, try loading the GeneFlow module:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>module load geneflow/latest
geneflow --help
</pre></div>
</div>
<p>If you need to install GeneFlow, the recommended method for installation is in a Python virtual environment, as described here: <a class="reference internal" href="../install.html#install-geneflow-venv"><span class="std std-ref">Install GeneFlow using a Python Virtual Environment</span></a>.</p>
<p>After installation in a Python virtual environment, load GeneFlow by sourcing the virtual environment:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>cd ~/geneflow_work
source gfpy/bin/activate
</pre></div>
</div>
</div>
</div>
<div class="section" id="clone-the-geneflow-app-template">
<h2>Clone the GeneFlow App Template<a class="headerlink" href="#clone-the-geneflow-app-template" title="Permalink to this headline">¶</a></h2>
<p>Create the “geneflow_work” directory in your home directory if it doesn’t already exist. This will be the location for all tutorial-related workflows, apps, and data:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>mkdir -p ~/geneflow_work
cd ~/geneflow_work
</pre></div>
</div>
<p>GeneFlow’s public Apps and Workflows repository is located here: <a class="reference external" href="https://gitlab.com/geneflow/">https://gitlab.com/geneflow/</a>. In addition to public apps and workflows, this repository contains app and workflow templates. When creating new GeneFlow apps or workflows, we recommend to start with the app or workflow template rather then from scratch. Clone the app template using git:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>git clone https://gitlab.com/geneflow/apps/app-template.git hello-world-gf
</pre></div>
</div>
<p>This command downloads the app template into the “hello-world-gf” directory. “hello-world-gf” also happens to be the name of the app you’re creating in this tutorial.</p>
<p>The GeneFlow app template contains a simple, but fully functional application. View the contents of the app template using the “tree” command:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>cd hello-world-gf
tree .
</pre></div>
</div>
<p>You should see the app template directory structure:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>.
├── assets
│   └── README.rst
├── build
│   └── README.rst
├── config.yaml
├── docs
│   └── README.rst
├── README.rst
└── test
    ├── data
    │   └── file.txt
    └── README.rst

5 directories, 7 files
</pre></div>
</div>
<p>You’ll need to update the “config.yaml” file to create the “Hello World” app. The “config.yaml” file is the main app configuration file, which defines the inputs, parameters, and execution commands of the app.</p>
<p>It’s good practice to also update the main “README.rst” file to document the app.</p>
</div>
<div class="section" id="configure-the-app">
<h2>Configure the App<a class="headerlink" href="#configure-the-app" title="Permalink to this headline">¶</a></h2>
<p>Configure the app by editing the “config.yaml” file. This file currently contains the configuration of a fully functional app, so you’ll be simplifying some of the sections to create the “hello-world” app. Open the “config.yaml” file using your favorite text editor (vi and nano examples shown):</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>vi ./config.yaml
</pre></div>
</div>
<p>or:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>nano ./config.yaml
</pre></div>
</div>
<p>The “config.yaml” file contains four main sections: Metadata, Inputs and Parameters, Execution Methods, and Assets. Edit each of these sections to create the “hello-world” app.</p>
<div class="section" id="metadata">
<h3>Metadata<a class="headerlink" href="#metadata" title="Permalink to this headline">¶</a></h3>
<p>The app metadata section contains the following basic information:</p>
<dl class="simple">
<dt>name:</dt><dd><p>Name of the GeneFlow app. We recommend to include version information if your app is wrapping a specific binary, container, or script. The app name should also include a “gf” suffix. For example, if the app is meant to wrap the “mem” function in BWA version 0.7.17, the app name should be “bwa-mem-0.7.17-gf”. For this example, use “hello-world-gf” without a version number because the app does not wrap a specific binary, container, or script.</p>
</dd>
<dt>description:</dt><dd><p>A title or short description of the app. For this example, use “Simple hello world GeneFlow app”.</p>
</dd>
<dt>repo_uri:</dt><dd><p>The full URL of the app’s source repository. This information is not available yet, so leave it blank.</p>
</dd>
<dt>version:</dt><dd><p>A string value that represents the app’s version. For this example, use “0.1”. We recommend to start with “0.1” for new apps and increment the number when changes are made to the app.</p>
</dd>
</dl>
<p>In the “config.yaml” file, modify the “Metadata” section so that it looks like the following:</p>
<div class="highlight-yaml notranslate"><div class="highlight"><pre><span></span><span class="c1"># name: standard GeneFlow app name</span>
<span class="nt">name</span><span class="p">:</span> <span class="l l-Scalar l-Scalar-Plain">hello-world-gf</span>
<span class="c1"># description: short description for the app</span>
<span class="nt">description</span><span class="p">:</span> <span class="l l-Scalar l-Scalar-Plain">Simple hello world GeneFlow app</span>
<span class="c1"># repo_uri: link to the app&#39;s git repo</span>
<span class="nt">repo_uri</span><span class="p">:</span>
<span class="c1"># version: must be incremented every time this file, or any file in the app</span>
<span class="c1"># project is modified</span>
<span class="nt">version</span><span class="p">:</span> <span class="s">&#39;0.1&#39;</span>
</pre></div>
</div>
</div>
<div class="section" id="inputs-and-parameters">
<h3>Inputs and Parameters<a class="headerlink" href="#inputs-and-parameters" title="Permalink to this headline">¶</a></h3>
<p>Each app input and parameter item is defined in a subsection with several properties. At least one input and one parameter is requred for each app. The “output” parameter is required, and must be manually included in the config file.</p>
<p>The example “Hello World” app doesn’t need any inputs. However, because at least one input is required, define a “dummy”, or un-used, input called “file”. Modify the “Inputs and Parameters” section of the “config.yaml” file so that it looks like the following:</p>
<div class="highlight-yaml notranslate"><div class="highlight"><pre><span></span><span class="nt">inputs</span><span class="p">:</span>
  <span class="nt">file</span><span class="p">:</span>
    <span class="nt">label</span><span class="p">:</span> <span class="l l-Scalar l-Scalar-Plain">Dummy Input File</span>
    <span class="nt">description</span><span class="p">:</span> <span class="l l-Scalar l-Scalar-Plain">Dummy input file</span>
    <span class="nt">type</span><span class="p">:</span> <span class="l l-Scalar l-Scalar-Plain">File</span>
    <span class="nt">required</span><span class="p">:</span> <span class="l l-Scalar l-Scalar-Plain">false</span>

<span class="nt">parameters</span><span class="p">:</span>
  <span class="nt">output</span><span class="p">:</span>
    <span class="nt">label</span><span class="p">:</span> <span class="l l-Scalar l-Scalar-Plain">Output Text File</span>
    <span class="nt">description</span><span class="p">:</span> <span class="l l-Scalar l-Scalar-Plain">Output text file</span>
    <span class="nt">type</span><span class="p">:</span> <span class="l l-Scalar l-Scalar-Plain">File</span>
    <span class="nt">required</span><span class="p">:</span> <span class="l l-Scalar l-Scalar-Plain">true</span>
    <span class="nt">test_value</span><span class="p">:</span> <span class="l l-Scalar l-Scalar-Plain">output.txt</span>
</pre></div>
</div>
<p>For a more detailed explanation of each input or parameter property, see <a class="reference internal" href="../apps.html#apps-inputs-parameters"><span class="std std-ref">App Inputs and Parameters</span></a>.</p>
</div>
<div class="section" id="execution-methods">
<h3>Execution Methods<a class="headerlink" href="#execution-methods" title="Permalink to this headline">¶</a></h3>
<p>The “Execution Methods” section of the app configuration file defines what the app actually does when executed. Apps can be defined with multiple execution methods. The specific method executed upon app invocation is either auto-detected or specified on the command line. Execution method names are customizable and the choice of a name should depend on the execution system. For example, if the app dependencies are installed globally in the execution system, use an execution method called “environment” (indicating that dependencies are available in the environment). If the app dependencies are containerized with Singularity, use an execution method called “singularity”. For a more detailed explanation of the app “Execution Methods” section, see <a class="reference internal" href="../apps.html#app-execution-methods"><span class="std std-ref">App Execution Methods</span></a>.</p>
<p>The “Execution Methods” section of the “config.yaml” file contains four sub-sections: “default_exec_method”, “pre_exec”, “exec_methods”, and “post_exec”.</p>
<p>The “default_exec_method” sub-section is a single string value. Set this to “auto”, indicating that the execution method should be auto-detected. Alternatively, you can set it to one of the execution methods defined in the “exec_methods” sub-section, e.g., “environment” or “singularity”.</p>
<div class="highlight-yaml notranslate"><div class="highlight"><pre><span></span><span class="nt">default_exec_method</span><span class="p">:</span> <span class="l l-Scalar l-Scalar-Plain">auto</span>
</pre></div>
</div>
<p>The “pre_exec” sub-section defines any commands that should be executed prior to commands in the main “exec_methods” sub-section. These usually include commands for directory or file preparation that are common for all execution methods, e.g., creating an output directory. For this tutorial, no “pre_exec” commands are required, so leave it blank:</p>
<div class="highlight-yaml notranslate"><div class="highlight"><pre><span></span><span class="nt">pre_exec</span><span class="p">:</span>
</pre></div>
</div>
<p>The “Hello World” app simply prints “Hello World!” to a text file using the standard Linux “echo” command. Thus, define a single execution method in the “exec_methods” sub-section called “environment”, which indicates that the needed commands or tools are already available in Linux. Update the “exec_methods” sub-section so that it looks like the following:</p>
<div class="highlight-yaml notranslate"><div class="highlight"><pre><span></span><span class="nt">exec_methods</span><span class="p">:</span>
<span class="p p-Indicator">-</span> <span class="nt">name</span><span class="p">:</span> <span class="l l-Scalar l-Scalar-Plain">environment</span>
  <span class="nt">if</span><span class="p">:</span>
  <span class="p p-Indicator">-</span> <span class="nt">in_path</span><span class="p">:</span> <span class="s">&#39;echo&#39;</span>
  <span class="nt">exec</span><span class="p">:</span>
  <span class="p p-Indicator">-</span> <span class="nt">run</span><span class="p">:</span> <span class="l l-Scalar l-Scalar-Plain">echo &#39;Hello World!&#39;</span>
    <span class="nt">stdout</span><span class="p">:</span> <span class="l l-Scalar l-Scalar-Plain">${OUTPUT_FULL}</span>
</pre></div>
</div>
<p>The “if” statement is used for auto-detecting the execution method. If multiple execution methods are specified, the first execution method with an “if” statement that evaluates to “True” will be selected for execution. In this example, the statement <code class="docutils literal notranslate"><span class="pre">in_path:</span> <span class="pre">'echo'</span></code> within the “if” statement means that the “environment” execution method will be selected if the “echo” command is available in the environment path. The “exec” statement contains a list of commands to be executed for the “environment” execution method. The “environment” execution method contains only a single command that echos the “Hello World!” text to an output file. Here, ${OUTPUT_FULL} is the full path of the file specified by the “output” parameter.</p>
<p>The “post_exec” sub-section defines any commands that should be executed after commands in the main “exec_methods” sub-section. These usually include commands for cleaning up any temporary files created during app execution. For this tutorial, no clean-up commands are necessary, so leave it blank:</p>
<div class="highlight-yaml notranslate"><div class="highlight"><pre><span></span><span class="nt">post_exec</span><span class="p">:</span>
</pre></div>
</div>
</div>
<div class="section" id="assets">
<h3>Assets<a class="headerlink" href="#assets" title="Permalink to this headline">¶</a></h3>
<p>The “assets” section of the “config.yaml” file specifies additional scripts, binaries, or containers that need to be cloned from a git repo, copied from another location, and/or built during app installation. In this example, the app is fully contained within the “Execution Methods” section, so no additional assets are required. Specify this in the assets section as follows:</p>
<div class="highlight-yaml notranslate"><div class="highlight"><pre><span></span><span class="nt">default_asset</span><span class="p">:</span> <span class="l l-Scalar l-Scalar-Plain">none</span>

<span class="nt">assets</span><span class="p">:</span>
  <span class="nt">none</span><span class="p">:</span> <span class="p p-Indicator">[]</span>
</pre></div>
</div>
</div>
</div>
<div class="section" id="make-the-app">
<h2>“Make” the App<a class="headerlink" href="#make-the-app" title="Permalink to this headline">¶</a></h2>
<p>Now that the app has been configured, generate the app wrapper script, the test script, and various definition files using the following commands:</p>
<p>First, make sure you’re still in the app directory:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>cd ~/geneflow_work/hello-world-gf
</pre></div>
</div>
<p>Then run the GeneFlow “make-app” command:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>geneflow make-app .
</pre></div>
</div>
<p>GeneFlow will then generate four files:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>2019-05-31 00:21:43 INFO [app_installer.py:267:make_def()] compiling /home/[user]/geneflow_work/hello-world-gf/app.yaml.j2
2019-05-31 00:21:43 INFO [app_installer.py:293:make_agave()] compiling /home/[user]/geneflow_work/hello-world-gf/agave-app-def.json.j2
2019-05-31 00:21:43 INFO [app_installer.py:325:make_wrapper()] compiling /home/[user]/geneflow_work/hello-world-gf/assets/hello-world-gf.sh
2019-05-31 00:21:43 INFO [app_installer.py:357:make_test()] compiling /home/[user]/geneflow_work/hello-world-gf/test/test.sh
</pre></div>
</div>
<p>Finally, make the app wrapper script executable:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>chmod +x ./assets/hello-world-gf.sh
</pre></div>
</div>
</div>
<div class="section" id="test-the-app">
<h2>Test the App<a class="headerlink" href="#test-the-app" title="Permalink to this headline">¶</a></h2>
<p>The GeneFlow “make-app” command generates a “test.sh” script inside the “test” folder. If your app requires test data, that data can be placed inside the “test” folder, ideally within a sub-folder called “data”. In this example, no test data is required.</p>
<p>To test the app, run the following commands:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>cd test
sh ./test.sh
</pre></div>
</div>
<p>You should see output similar to the following:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>CMD=/home/[user]/geneflow_work/hello-world-gf/test/../assets/hello-world-gf.sh --output=&quot;output.txt&quot; --exec_method=&quot;auto&quot;
File:
Output: output.txt
Execution Method: auto
Detected Execution Method: environment
CMD=echo &#39;Hello World!&#39;  &gt;&quot;/home/[user]/geneflow_work/hello-world-gf/test/output.txt&quot;
Exit code: 0
Exit code: 0
</pre></div>
</div>
<p>The “output.txt” file should also have been created in the test directory with the text “Hello World!”. View it with:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>cat ./output.txt
</pre></div>
</div>
<p>And you should see this output:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>Hello World!
</pre></div>
</div>
</div>
<div class="section" id="update-the-app-readme">
<h2>Update the App README<a class="headerlink" href="#update-the-app-readme" title="Permalink to this headline">¶</a></h2>
<p>It is best practice to update the app README file to include the app name, a short description, and descriptions for each input and parameter. Edit the README.rst file in the main app directory:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>cd ~/geneflow_work/hello-world-gf
vi ./README.rst
</pre></div>
</div>
<p>Modify the file so it looks like the following:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>Hello World! Basic GeneFlow App
===============================

Version: 0.1

This is a basic GeneFlow app.

Inputs
------

1. file: Dummy input file, use any small file.

Parameters
----------

1. output: Output text file where &quot;Hello World!&quot; will be printed.
</pre></div>
</div>
<p>Save the file and exit the editor.</p>
</div>
<div class="section" id="commit-the-app-to-a-git-repo">
<h2>Commit the App to a Git Repo<a class="headerlink" href="#commit-the-app-to-a-git-repo" title="Permalink to this headline">¶</a></h2>
<p>Finally, commit the app to a git repo so that it can be used in a GeneFlow workflow. First, if you don’t already have one, create an account in either GitHub, GitLab, BitBucket, or your company/organization’s git repository. Delete the output file that was created while testing the app, since this output file is not part of the main app definition:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>cd ~/geneflow_work/hello-world-gf
rm ./test/output.txt
</pre></div>
</div>
<p>Commit all changes to the local git repo and tag the app version:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>git add -A
git commit -m &quot;initial version of the hello world app&quot;
git tag 0.1
</pre></div>
</div>
<p>Push to the remote repo using the following commands, depending on where your repository is located.</p>
<div class="section" id="github">
<h3>GitHub<a class="headerlink" href="#github" title="Permalink to this headline">¶</a></h3>
<p>If your repository is in GitHub, you must first create the repo on the GitHub.com site. Once created, it will likely be located at a URL similar to <code class="docutils literal notranslate"><span class="pre">https://github.com/[user]/hello-world-gf.git</span></code>, where <code class="docutils literal notranslate"><span class="pre">[user]</span></code> should be replaced with your GitHub username or group. Push your code to GitHub using the following commands:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>git remote set-url origin https://github.com/[user]/hello-world-gf.git
git push --tags origin master
</pre></div>
</div>
<p>Be sure to replace <code class="docutils literal notranslate"><span class="pre">[user]</span></code> with your GitHub username or group.</p>
</div>
<div class="section" id="gitlab">
<h3>GitLab<a class="headerlink" href="#gitlab" title="Permalink to this headline">¶</a></h3>
<p>If your repository is in GitLab, you don’t need to create the repo on the GitLab.com site. You can skip directly to pushing your code to the git URL, which will be similar to <code class="docutils literal notranslate"><span class="pre">https://gitlab.com/[user]/hello-world-gf.git</span></code>, where <code class="docutils literal notranslate"><span class="pre">[user]</span></code> should be replaced with your GitLab username or group:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>git remote set-url origin https://gitlab.com/[user]/hello-world-gf.git
git push --tags origin master
</pre></div>
</div>
<p>Be sure to replace <code class="docutils literal notranslate"><span class="pre">[user]</span></code> with your GitLab username or group.</p>
</div>
<div class="section" id="organization-gitlab">
<h3>Organization GitLab<a class="headerlink" href="#organization-gitlab" title="Permalink to this headline">¶</a></h3>
<p>If you have a company or organization GitLab server, your git repo hostname will likely be different. For example, it could be hosted at <code class="docutils literal notranslate"><span class="pre">https://git.biotech.cdc.gov/[user]/hello-world-gf.git</span></code>, where <code class="docutils literal notranslate"><span class="pre">[user]</span></code> should be replaced with your username or group:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>git remote set-url origin https://git.biotech.cdc.gov/[user]/hello-world-gf.git
git push --tags origin master
</pre></div>
</div>
<p>Be sure to replace <code class="docutils literal notranslate"><span class="pre">[user]</span></code> with your organization’s GitLab username or group.</p>
</div>
</div>
<div class="section" id="summary">
<h2>Summary<a class="headerlink" href="#summary" title="Permalink to this headline">¶</a></h2>
<p>Congratulations! You created a basic GeneFlow app, tested it using the auto-generated test script, and committed it to a git repo. The next tutorial covers creation of a one-step GeneFlow workflow that uses this “Hello-World” app.</p>
</div>
</div>


          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
  <h3><a href="../../index.html">Table of Contents</a></h3>
  <ul>
<li><a class="reference internal" href="#">Basic App: Hello World</a><ul>
<li><a class="reference internal" href="#configure-the-environment">Configure the Environment</a><ul>
<li><a class="reference internal" href="#load-or-install-geneflow">Load or Install GeneFlow</a></li>
</ul>
</li>
<li><a class="reference internal" href="#clone-the-geneflow-app-template">Clone the GeneFlow App Template</a></li>
<li><a class="reference internal" href="#configure-the-app">Configure the App</a><ul>
<li><a class="reference internal" href="#metadata">Metadata</a></li>
<li><a class="reference internal" href="#inputs-and-parameters">Inputs and Parameters</a></li>
<li><a class="reference internal" href="#execution-methods">Execution Methods</a></li>
<li><a class="reference internal" href="#assets">Assets</a></li>
</ul>
</li>
<li><a class="reference internal" href="#make-the-app">“Make” the App</a></li>
<li><a class="reference internal" href="#test-the-app">Test the App</a></li>
<li><a class="reference internal" href="#update-the-app-readme">Update the App README</a></li>
<li><a class="reference internal" href="#commit-the-app-to-a-git-repo">Commit the App to a Git Repo</a><ul>
<li><a class="reference internal" href="#github">GitHub</a></li>
<li><a class="reference internal" href="#gitlab">GitLab</a></li>
<li><a class="reference internal" href="#organization-gitlab">Organization GitLab</a></li>
</ul>
</li>
<li><a class="reference internal" href="#summary">Summary</a></li>
</ul>
</li>
</ul>

  <h4>Previous topic</h4>
  <p class="topless"><a href="../tutorials.html"
                        title="previous chapter">GeneFlow Tutorials</a></p>
  <h4>Next topic</h4>
  <p class="topless"><a href="one-step-workflow.html"
                        title="next chapter">One-Step Workflow: Hello World</a></p>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="../../_sources/chapters/tutorials/basic-app.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3>Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="../../search.html" method="get">
      <input type="text" name="q" />
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script type="text/javascript">$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../../genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="one-step-workflow.html" title="One-Step Workflow: Hello World"
             >next</a> |</li>
        <li class="right" >
          <a href="../tutorials.html" title="GeneFlow Tutorials"
             >previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="../../index.html">GeneFlow 1.11.0 documentation</a> &#187;</li>
          <li class="nav-item nav-item-1"><a href="../tutorials.html" >GeneFlow Tutorials</a> &#187;</li> 
      </ul>
    </div>
    <div class="footer" role="contentinfo">
        &#169; Copyright 2019, SCBS.
      Created using <a href="http://sphinx-doc.org/">Sphinx</a> 2.0.1.
    </div>
  </body>
</html>