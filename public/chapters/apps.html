
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8" />
    <title>Creating GeneFlow Apps &#8212; GeneFlow 1.11.0 documentation</title>
    <link rel="stylesheet" href="../_static/classic.css" type="text/css" />
    <link rel="stylesheet" href="../_static/pygments.css" type="text/css" />
    
    <script type="text/javascript" id="documentation_options" data-url_root="../" src="../_static/documentation_options.js"></script>
    <script type="text/javascript" src="../_static/jquery.js"></script>
    <script type="text/javascript" src="../_static/underscore.js"></script>
    <script type="text/javascript" src="../_static/doctools.js"></script>
    <script type="text/javascript" src="../_static/language_data.js"></script>
    
    <link rel="index" title="Index" href="../genindex.html" />
    <link rel="search" title="Search" href="../search.html" />
    <link rel="next" title="GeneFlow Tutorials" href="tutorials.html" />
    <link rel="prev" title="GeneFlow Definition Language" href="definition.html" /> 
  </head><body>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="tutorials.html" title="GeneFlow Tutorials"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="definition.html" title="GeneFlow Definition Language"
             accesskey="P">previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="../index.html">GeneFlow 1.11.0 documentation</a> &#187;</li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <div class="section" id="creating-geneflow-apps">
<h1>Creating GeneFlow Apps<a class="headerlink" href="#creating-geneflow-apps" title="Permalink to this headline">¶</a></h1>
<p>GeneFlow apps can be created and configured by editing the app’s config.yaml file. An example config file can be found in the GeneFlow app template.</p>
<p>Clone the template to your new app directory with the following command (Note: This repository is currently only available in the CDC GitLab repository):</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>git clone https://git.biotech.cdc.gov/geneflow-apps/app-template.git new-app
</pre></div>
</div>
<p>Replace the folder name, ‘new-app’, with the name of your app. The app template contains the following file and directory structure:</p>
<div class="highlight-none notranslate"><div class="highlight"><pre><span></span>├── assets
│       └── bwa-mem-0.7.17-gf.sh
├── build
│   └── README.rst
├── docs
│   └── README.rst
├── test
│   ├── data
│   └── test.sh
├── agave-app-def.json.j2
├── app.yaml.j2
├── config.yaml
└── README.rst
</pre></div>
</div>
<p>The ‘assets’ folder contains the app wrapper script, which is auto-generated based on the config.yaml file. The wrapper script is the app’s execution entrypoint and contains logic to execute app binaries, scripts, or containers. Any binaries, scripts, or containers that need to be packaged with the app should be placed in the ‘assets’ folder. However, we recommend to containerize the app and publish it to a public repository or shared location.</p>
<p>The optional ‘build’ folder contains scripts to build app binaries or containers upon app installation. Build scripts may also be placed in source control repositories and referenced by the config.yaml file.</p>
<p>The ‘docs’ folder contains detailed app documentation, including information about inputs and parameters.</p>
<p>The ‘test’ folder contains a ‘data’ folder with data used to test the app, and a shell script named ‘test.sh’. The ‘test.sh’ script is auto-generated based on the config.yaml file.</p>
<p>The ‘agave-app-def.json.j2’ and ‘app.yaml.j2’ files are also auto-generated based on the config.yaml file, and represent the Agave and GeneFlow app definitions.</p>
<p>The ‘README.rst’ file should contain a brief description of your app and is automatically rendered in the source code repository.</p>
<p>Finally, ‘config.yaml’ is the primary app config file, which is described in detail below. Note that in this example app template, the wrapper script, ‘bwa-mem-0.7.17-gf.sh’ will be auto-generated with a different name based on your app configuration. Thus, the ‘bwa-mem-0.7.17-gf.sh’ should be deleted prior to committing your app to a source control repository. Similarly, the ‘test/data’ folder contains sample data specific to the bwa-mem app, and may also be deleted or replaced with custom data for your app.</p>
<div class="section" id="geneflow-app-config-file">
<h2>GeneFlow App Config File<a class="headerlink" href="#geneflow-app-config-file" title="Permalink to this headline">¶</a></h2>
<p>The GeneFlow app config file, named ‘config.yaml’ in the app repository, contains the following sections:</p>
<ol class="arabic simple">
<li><p>Metadata</p></li>
<li><p>Inputs and Parameters</p></li>
<li><p>App Execution Methods</p></li>
<li><p>App Assets</p></li>
</ol>
<p>In addition, the YAML file requires specific syntax to define execution blocks and conditional blocks.</p>
<div class="section" id="metadata">
<h3>Metadata<a class="headerlink" href="#metadata" title="Permalink to this headline">¶</a></h3>
<p>The app metadata section contains name, description, and source information. Metadata is used to populate the GeneFlow app definition file, app.yaml.</p>
<dl class="simple">
<dt>name:</dt><dd><p>Name of the GeneFlow app. We recommend to include version information for the app binary/container and a ‘gf’ suffix in the app name. For example, if the app is meant to wrap the ‘mem’ function in BWA version 0.7.17, the app name should be ‘bwa-mem-0.7.17-gf’.</p>
</dd>
<dt>description:</dt><dd><p>A title or short description of the app.</p>
</dd>
<dt>repo_uri:</dt><dd><p>The full URL of the app’s source repository.</p>
</dd>
<dt>version:</dt><dd><p>A string value that represents the app’s version.</p>
</dd>
</dl>
</div>
<div class="section" id="inputs-and-parameters">
<span id="apps-inputs-parameters"></span><h3>Inputs and Parameters<a class="headerlink" href="#inputs-and-parameters" title="Permalink to this headline">¶</a></h3>
<p>Each app input and parameter item is defined in a subsection with the same name as the input/parameter. The ‘output’ parameter is required, and must be manually included in the config file. See the following section for details about the ‘output’ parameter. Each input or parameter subsection must have the following fields:</p>
<dl>
<dt>label:</dt><dd><p>A title or short description of the field.</p>
</dd>
<dt>description:</dt><dd><p>A long description of the field.</p>
</dd>
<dt>type:</dt><dd><p>Data type of the field. For input fields, values can be: File, Directory, or Any. For parameter fields, values can be: File, Directory, Any, string, int, float, double, or long.</p>
</dd>
<dt>required:</dt><dd><p>Specifies if the input/parameter is required.</p>
</dd>
<dt>default:</dt><dd><p>Specifies the default value of the field in the Agave or app definition, which would then be passed to the wrapper script.</p>
<blockquote>
<div><p>Note: The following rules apply when handling ‘required’ and ‘default’ fields:</p>
<ol class="arabic simple">
<li><p>If required == true, the default value is ignored.</p></li>
<li><p>If required == false, and default is specified, the input/parameter is set to the default value in the Agave and app definitions.</p></li>
<li><p>If required == false, and default is NOT specified, the input/parameter is only set if corresponding args are passed to the app.</p></li>
</ol>
</div></blockquote>
</dd>
<dt>script_default:</dt><dd><p>Only for inputs. Specifies the default value of the field in the wrapper script and is over-written by the resolved “default” value.</p>
<blockquote>
<div><p>Note: The following rules apply when handling ‘required’ and ‘script_default’ fields:</p>
<ol class="arabic simple">
<li><p>If required == true, the script_default value is ignored.</p></li>
<li><p>If required == false, and script_default is specified, the input is set to the default value in the wrapper script before arg parsing and allowed to be over-written by any args passed to the wrapper script.</p></li>
<li><p>If required == false, and script_default is NOT specified, the input is only set if corresponding args are passed to the wrapper script.</p></li>
</ol>
</div></blockquote>
</dd>
<dt>test_value:</dt><dd><p>(Optional) If specified, the input/parameter is set to this value in the test script.</p>
</dd>
<dt>post_exec:</dt><dd><p>(Optional) List of shell/bash commands for post-processing of the input/parameter value after argument parsing. These commands modify or check the value of inputs/parameters; or create additional shell/bash variables for use in later parts of the script. By default, for ‘File’, ‘Directory’, or ‘Any’ types, the following commands are included in the wrapper script before any items listed in post_exec. If the name of the input/parameter is ‘varname’, then the following lines are added to the wrapper script:</p>
<blockquote>
<div><div class="highlight-bash notranslate"><div class="highlight"><pre><span></span><span class="nv">VARNAME_FULL</span><span class="o">=</span><span class="k">$(</span>readlink -f <span class="si">${</span><span class="nv">VARNAME</span><span class="si">}</span><span class="k">)</span>
<span class="nv">VARNAME_DIR</span><span class="o">=</span><span class="k">$(</span>dirname <span class="si">${</span><span class="nv">VARNAME_FULL</span><span class="si">}</span><span class="k">)</span>
<span class="nv">VARNAME_BASE</span><span class="o">=</span><span class="k">$(</span>basename <span class="si">${</span><span class="nv">VARNAME_FULL</span><span class="si">}</span><span class="k">)</span>
</pre></div>
</div>
</div></blockquote>
<p>See the section ‘Execution Blocks’ for information about the required format for execution blocks.</p>
</dd>
</dl>
</div>
<div class="section" id="app-output">
<h3>App Output<a class="headerlink" href="#app-output" title="Permalink to this headline">¶</a></h3>
<p>The app ‘output’ parameter is associated with two additional variables for storing logs and temporary files:</p>
<blockquote>
<div><div class="highlight-bash notranslate"><div class="highlight"><pre><span></span><span class="nv">LOG_FULL</span><span class="o">=</span><span class="s2">&quot;</span><span class="si">${</span><span class="nv">OUTPUT_DIR</span><span class="si">}</span><span class="s2">/_log&quot;</span>
<span class="nv">TMP_FULL</span><span class="o">=</span><span class="s2">&quot;</span><span class="si">${</span><span class="nv">OUTPUT_DIR</span><span class="si">}</span><span class="s2">/_tmp&quot;</span>
</pre></div>
</div>
</div></blockquote>
<p>The LOG_FULL variable points to a directory that, once created, persists in the workflow intermediate and final output directory. LOG_FULL is optional, and must be manually created with a ‘mkdir’ command within the app config file preior to use. The ‘_log’ directory must be accounted for when executing ‘map’ steps that process input folders. To exclude, a look-ahead regex can be used to filter the folder.</p>
<p>The TMP_FULL variable must also be manually created, but also must be manually deleted within the “clean-up” section of the app configuration. The TMP_FULL directory may or may not persist in the workflow intermediate and output directory, depending on the execution context.</p>
</div>
<div class="section" id="app-execution-methods">
<span id="id1"></span><h3>App Execution Methods<a class="headerlink" href="#app-execution-methods" title="Permalink to this headline">¶</a></h3>
<p>Apps can be defined with multiple execution methods, with a single method being specified upon app execution. Execution methods define the medium of execution (i.e., singularity, docker, binary, script), as well as the location of the execution assets (i.e., included as part of the app package, in a shared location, from a repository, or pre-loaded/available in the environment PATH).</p>
<p>This section of the config file includes the following fields and sub-sections:</p>
<dl>
<dt>default_exec_method:</dt><dd><p>This specifies the default execution method, which should be one of the items listed in the ‘exec_methods’ section below. Alternatively, a value of ‘auto’ means that the execution method is automatically detected by the wrapper script based on the ‘if’ conditions specified within each ‘exec_methods’ section.</p>
</dd>
<dt>pre_exec:</dt><dd><p>This section contains a list of execution commands for environment preparation to be executed before any method-specific execution commands. Each pre_exec item is an execution block, as defined in the “Execution Blocks” section.</p>
</dd>
<dt>exec_methods:</dt><dd><p>This section contains a list of execution methods, with each list item containing the following:</p>
<blockquote>
<div><ol class="arabic simple">
<li><p>name: The name of the execution method, which can be one of the following or a custom method: singularity, docker, cdc-shared-singularity, environment, module.</p></li>
<li><p>if: A conditional block, used to auto-detect the execution method. Each execution method conditional block is checked in the order of the listed execution method, and the first execution method with a satisfied condition is selected. See the “Conditional Blocks” section for more information.</p></li>
<li><p>exec: A list of execution blocks to be executed if the method’s condition is satisified. See the “Execution Blocks” section for more information.</p></li>
</ol>
</div></blockquote>
</dd>
<dt>post_exec:</dt><dd><p>This section contains a list of execution commands for environment cleanup to be executed after any method-specific execution commands. Each post_exec item is an execution block, as defined in the “Execution Blocks” section.</p>
</dd>
</dl>
</div>
<div class="section" id="app-assets">
<h3>App Assets<a class="headerlink" href="#app-assets" title="Permalink to this headline">¶</a></h3>
<p>App assets are additional scripts, binaries, or containers that need to be cloned from a git repo, copied from another location, and/or built during app installation.</p>
<p>The app assets section of the config file should contain the following items:</p>
<dl>
<dt>default_asset:</dt><dd><p>The default asset to install if none is specified.</p>
</dd>
<dt>assets:</dt><dd><p>The assets section can have multiple sub-sections, with no strict naming convention. Each section encompasses a single app asset and contains an array, with each array element defined with the following:</p>
<blockquote>
<div><ol class="arabic simple">
<li><p>type: value can be “copy” or “build”.</p></li>
<li><p>src: Source of assets. If type == copy, it must be relative to “prefix”, which is passed to the install script. If type == build, it must be relative to the base app package directory and “build” must be the first folder name. src can include wild-cards, e.g., /folder/*, but if wildcards are specified, zip must be disabled.</p></li>
<li><p>dst: Destination of assets. This is relative to the base app package directory, and “assets” must be the first folder name.</p></li>
<li><p>zip: if present, src files are tar.gz zipped prior to copying to destination. src must be a folder without wildcards if zipping.</p></li>
<li><p>repo: if type == build, repo specifies the source repository to be cloned into the “build” directory.</p></li>
<li><p>tag: if repo is specified and type == build, tag is the branch or tag to be cloned.</p></li>
<li><p>folder: folder to which repo should be cloned. If “repo” is omitted, “folder” must be present. If so, “folder” refers to a folder inside the app “build” directory that contains build scripts. “folder” is useful when build scripts need to be included as part of the app package (instead of in a separate repo).</p></li>
</ol>
</div></blockquote>
</dd>
</dl>
<p>“build” type assets, whether cloned from a git repo, or included as part of the app package must include a “Makefile” with a default build target.</p>
</div>
<div class="section" id="execution-blocks">
<h3>Execution Blocks<a class="headerlink" href="#execution-blocks" title="Permalink to this headline">¶</a></h3>
<p>Execution blocks occur in input/parameter post processing sections (i.e., post_exec), as well as app pre (i.e., pre_exec), post (i.e., post_exec), and method-specific (i.e., exec_methods.exec) execution sections. Regardless of the location, all execution blocks are similarly formatted. Each of these sections is an array, with each array item defined with the following fields:</p>
<dl>
<dt>if:</dt><dd><p>(Optional) Condition that must be satisfied for the item to be executed. See the section “Conditional Blocks” for more information.</p>
</dd>
<dt>else:</dt><dd><p>(Optional) If the “if” condition is present, and “else” is present, items in the “else” block are executed only if the “if” condition is not satisfied.</p>
</dd>
<dt>pipe:</dt><dd><p>(Optional) If included, all remaining fields at this level are ignored. The pipe field is an array, with each array item containing an execution item. The order of execution items within “pipe” are piped in order of appearance. STDOUT is piped from one execution command to the next. Thus, within pipe execution items, the “stdout” field is ignored. Nested “pipe” fields are also ignored, preventing recursive piping.</p>
</dd>
<dt>multi:</dt><dd><p>(Optional) If included, all remaining fields at this level are ignored. The multi field is an array, with each array item containing an execution item. Each included execution item can be a pipe, or another multi, allowing for nested execution.</p>
</dd>
<dt>type:</dt><dd><p>(Optional) Valid values are ‘shell’, ‘singularity’, and ‘docker’. If omiitted, the default value is ‘shell’. This specifies the type of execution.</p>
</dd>
<dt>run:</dt><dd><p>Command to run. If type is singularity or docker, this is the command passed to the container executor after the container image is specified.</p>
</dd>
<dt>options:</dt><dd><p>Container entrypoint command. If type is singularity or docker, this command is the singularity or docker sub-command and options. For singularity, the default is ‘-s exec’. For docker, the default is ‘run –rm’.</p>
</dd>
<dt>image:</dt><dd><p>If type is singularity or docker, this is the path, url, or name of the container.</p>
</dd>
<dt>args:</dt><dd><p>Optional arguments to be passed to the command. This is expected to be an array, with each array item defined as follows:</p>
<blockquote>
<div><ol class="arabic simple">
<li><p>flag: (Optional) If present, the argument is pre-pended with this string.</p></li>
<li><p>mount: (Optional) If present, and type is singularity or docker, the value should be the bash variable name representing one of the inputs or file/directory parameters. For example, an input of “filename” should be represented as “${FILENAME}”. The file or directory’s containing directory is mounted to the container using the option: “${FILENAME_DIR}:/dataX”. If “value” is not specified, a value of “/dataX/${FILENAME_BASE}” is passed as an argument to the image. If “value” is present, the value is passed as an argument as follows: “/dataX/[value]”</p></li>
<li><p>value: (Optional) If present, used as the argument value. If “mount” is also present, see above rules for “mount”.</p></li>
</ol>
<p>Note that all “args” values are optional, and if none are specified, the argument is ignored.</p>
</div></blockquote>
</dd>
<dt>stdout:</dt><dd><p>(Optional) If present, the command’s standard output will be piped here.</p>
</dd>
<dt>stderr:</dt><dd><p>(Optional) If present, the command’s standard error will be piped here.</p>
</dd>
</dl>
<p>All bash/shell commands in the “exec_methods” section has access to a number of pre-defined variables, including:</p>
<blockquote>
<div><ol class="arabic simple">
<li><p>${SINGULARITY}: set to “yes” or “no” depending on whether the “singularity” binary was detected.</p></li>
<li><p>${DOCKER}: set to “yes” or “no” depending on whether the “docker” binary was detected.</p></li>
<li><p>${SCRIPT_DIR}: directory of the wrapper script, which may not be the current directory. This depends on the execution environment.</p></li>
<li><p>${VARNAME}: One for each input/parameter, set to value of the input/parameter.</p></li>
<li><p>${VARNAME_FULL}: if input/parameter is a File, Directory, or Any, this is the full path of the input/parameter.</p></li>
<li><p>${VARNAME_DIR}: if input/parameter is a File, Directory, or Any, this is the parent directory of the input/parameter.</p></li>
<li><p>${VARNAME_BASE}: if input/parameter is a File, Directory, or Any, this is the basename of the input/parameter.</p></li>
<li><p>${LOG_FULL}: location to store log files.</p></li>
<li><p>${TMP_FULL}: location to store temporary files.</p></li>
</ol>
</div></blockquote>
<p>Any additional bash/shell variables defined in the “post” section of each input/parameter, or defined in the “pre_exec” section are also available.</p>
</div>
<div class="section" id="conditional-blocks">
<h3>Conditional Blocks<a class="headerlink" href="#conditional-blocks" title="Permalink to this headline">¶</a></h3>
<p>Conditional blocks are nestable conditional tests that can be included in execution blocks. Test conditions can be grouped with the following section keywords:</p>
<dl class="simple">
<dt>all:</dt><dd><p>All items in this section must be satisified (i.e., [a AND b .. ]).</p>
</dd>
<dt>any:</dt><dd><p>At least one item in this section must be satisfied (i.e., [a OR b .. ]).</p>
</dd>
<dt>none:</dt><dd><p>None of the items in this section must be satisified (i.e., NOT [a AND b ..]).</p>
</dd>
</dl>
<p>These can be nested to any depth. Within these groups, test conditions can include the following, and parameters are passed as values (if single operand), or arrays (if two operands). Shell equivalent tests are shown below:</p>
<dl class="simple">
<dt>defined:</dt><dd><p>-n value</p>
</dd>
<dt>not_defined:</dt><dd><p>-z value</p>
</dd>
<dt>str_equal:</dt><dd><p>value[0] = value[1]</p>
</dd>
<dt>not_str_equal:</dt><dd><p>value[0] != value[1]</p>
</dd>
<dt>equal:</dt><dd><p>value[0] -eq value[1]</p>
</dd>
<dt>not_equal:</dt><dd><p>value[0] -ne value[1]</p>
</dd>
<dt>less:</dt><dd><p>value[0] -lt value[1]</p>
</dd>
<dt>greater:</dt><dd><p>value[0] -gt value[1]</p>
</dd>
<dt>less_equal:</dt><dd><p>value[0] -le value[1]</p>
</dd>
<dt>greater_equal:</dt><dd><p>value[0] -ge value[1]</p>
</dd>
<dt>file_exist:</dt><dd><p>-f value</p>
</dd>
<dt>not_file_exist:</dt><dd><p>! -f value</p>
</dd>
<dt>dir_exist:</dt><dd><p>-d value</p>
</dd>
<dt>not_dir_exist:</dt><dd><p>! -d value</p>
</dd>
<dt>exist:</dt><dd><p>-e value</p>
</dd>
<dt>not_exist:</dt><dd><p>! -e value</p>
</dd>
<dt>in_path:</dt><dd><p>command -v value &gt;/dev/null 2&gt;&amp;1</p>
</dd>
<dt>str_contain:</dt><dd><p>contains value[0] value[1]</p>
</dd>
<dt>not_str_contain:</dt><dd><p>! contains value[0] value[1]</p>
</dd>
</dl>
<p>Note that ‘contains’ is a function that tests for sub-strings. ‘contains’ evaluates to true (or 1) if value[1] is a sub-string of value[0]. All test conditions and section keywords must be list items. For example:</p>
<div class="highlight-none notranslate"><div class="highlight"><pre><span></span>if:
- all:
  - defined: &#39;${VALUE}&#39;
  - str_equal: [&#39;${VALUE}&#39;, &#39;val&#39;]
</pre></div>
</div>
</div>
</div>
<div class="section" id="generating-a-geneflow-app">
<h2>Generating a GeneFlow App<a class="headerlink" href="#generating-a-geneflow-app" title="Permalink to this headline">¶</a></h2>
<p>Once the app ‘config.yaml’ file has been defined, the app can be generated. The app generation process creates the wrapper script, Agave definition, GeneFlow definition, and test script. To generate the app, run the following command from within the app directory:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>geneflow make-app .
</pre></div>
</div>
</div>
</div>


          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
  <h3><a href="../index.html">Table of Contents</a></h3>
  <ul>
<li><a class="reference internal" href="#">Creating GeneFlow Apps</a><ul>
<li><a class="reference internal" href="#geneflow-app-config-file">GeneFlow App Config File</a><ul>
<li><a class="reference internal" href="#metadata">Metadata</a></li>
<li><a class="reference internal" href="#inputs-and-parameters">Inputs and Parameters</a></li>
<li><a class="reference internal" href="#app-output">App Output</a></li>
<li><a class="reference internal" href="#app-execution-methods">App Execution Methods</a></li>
<li><a class="reference internal" href="#app-assets">App Assets</a></li>
<li><a class="reference internal" href="#execution-blocks">Execution Blocks</a></li>
<li><a class="reference internal" href="#conditional-blocks">Conditional Blocks</a></li>
</ul>
</li>
<li><a class="reference internal" href="#generating-a-geneflow-app">Generating a GeneFlow App</a></li>
</ul>
</li>
</ul>

  <h4>Previous topic</h4>
  <p class="topless"><a href="definition.html"
                        title="previous chapter">GeneFlow Definition Language</a></p>
  <h4>Next topic</h4>
  <p class="topless"><a href="tutorials.html"
                        title="next chapter">GeneFlow Tutorials</a></p>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="../_sources/chapters/apps.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3>Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="../search.html" method="get">
      <input type="text" name="q" />
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script type="text/javascript">$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="tutorials.html" title="GeneFlow Tutorials"
             >next</a> |</li>
        <li class="right" >
          <a href="definition.html" title="GeneFlow Definition Language"
             >previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="../index.html">GeneFlow 1.11.0 documentation</a> &#187;</li> 
      </ul>
    </div>
    <div class="footer" role="contentinfo">
        &#169; Copyright 2019, SCBS.
      Created using <a href="http://sphinx-doc.org/">Sphinx</a> 2.0.1.
    </div>
  </body>
</html>